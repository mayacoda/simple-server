# Simple Server

A simple [Express](https://expressjs.com/) web server and [React](https://reactjs.org/) frontend application with a single API end point which returns a list of animals.

## Installation
```bash
npm install
```

## Run
To start the server, run the following command. The server listens to `localhost:4000` by default.
```bash
npm run start:server
```

To compile the frontend application, run the following command. The frontend is served on `localhost:3000` and can be accessed through the browser.
```bash
npm run start:client
```

## Build
The server is configured to serve static files from the `./build` directory, which on project load only has an index file with instructions on how to build the project.

In order to serve the compiled React application, run `npm build` and go to `localhost:4000`.