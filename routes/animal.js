const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');

router.get('/', function (req, res) {
    fs.readFile(path.resolve(__dirname, '../public/assets/animals.json'), 'utf-8', (err, file) => {
        if (err) {
            res.status(404);
            res.send(err);
            return;
        }

        try {
            const json = JSON.parse(file);
            res.status(200);
            res.send(json);
        } catch (ex) {
            res.status(500);
            res.send(ex);
        }
    });
});

module.exports = router;