import React, {Component} from 'react';
import './App.css';

class App extends Component {
    state = {
        animals: [],
        err: {},
        pending: false
    };

    getAnimals() {
        this.setState({pending: true});
        fetch("http://localhost:4000/animals").then(res => res.json()).then(animals => {
            this.setState({animals: animals, err: null, pending: false});
        }, err => {
            this.setState({animals: [], err: err, pending: false})
        });
    }

    render() {
        return (
            <div className="container">
                <button className="load-btn" onClick={(e) => this.getAnimals(e)}>Load Animals
                </button>

                {this.state.pending ? (<span>Loading...</span>) : ""}

                {this.state.err ? (<span>{this.state.err.message}</span>) :
                    (<ul className="animals-list">
                        {this.state.animals.map(a => {
                            return (<li key={a.name}>
                                {a.name}
                            </li>)
                        })}
                    </ul>)
                }
            </div>
        );
    }
}

export default App;
